#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import json

def check_over_heat(df):
    table = df[df['red-high-limit'] < df['raw-value']]
    overheat_count = table['red-high-limit'].rolling(window=5,min_periods=3).count()
    return overheat_count[overheat_count >= 3].index.values

def check_low_battery(df):
    table = df[df['red-low-limit'] > df['raw-value']]
    lowbat_count = table['red-low-limit'].rolling(window=5,min_periods=3).count()
    return lowbat_count[lowbat_count >= 3].index.values

def format_alert(alert):
    if alert['component'] == 'TSTAT':
        severity = 'RED HIGH'
    elif alert['component'] == 'BATT':
        severity = 'RED LOW'
    else:
        severity = 'None'
    alert_format = {
        "satelliteId": alert['satellite-id'],
        "severity": severity,
        "component": alert['component'],
        ##timestamp format example: "2018-01-01T23:01:38.001Z"
        "timestamp": alert['timestamp'].strftime('%Y-%m-%dT%X.%f')[:-3]+'Z'
    }
    return alert_format

## read data in
filename = input(prompt='Enter a filepath')
#filename = 'sample_input.txt'
data = pd.read_csv(filename, sep='|', header=None)

## apply headers to data
headers = '<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>'
data.columns = headers.replace('>','').replace('<','').split('|')

## convert timestamp strings to timestamp class
data['timestamp'] = pd.to_datetime(data['timestamp'],yearfirst=True)

## sort by time and set index to time
time_data = data.sort_values('timestamp', ascending=False).set_index('timestamp')

## check for instances where satellites overheat
heat_data = time_data.groupby(['satellite-id','component']).apply(check_over_heat)

## check for instances where satellites battery is low
battery_data = time_data.groupby(['satellite-id','component']).apply(check_low_battery)

## combine data sets and reset/ rename index
combined_df = pd.concat([heat_data,battery_data]).reset_index().rename(columns={0:'timestamp'})

## convert dataframe to dict
alert_dict = combined_df.to_dict(orient='index')

## match alerts to records in the original data
alert_df = pd.concat([data.loc[(data[list(match)] == pd.Series(match)).all(axis=1)] for match in alert_dict.values()])

## convert dataframe entries to alert messeges
alerts = [format_alert(dict(value)) for value in alert_df.reset_index().to_dict(orient='index').values()]

## print alerts
print(json.dumps(alerts, indent=1))